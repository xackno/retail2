<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ventas extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'usuario',
        'id_turno',
        'caja',
        'efectivo',
        'total_venta',
        'cantidad_pro',
        'id_productos',
        'precio_vendido',
        'created_at',
        'updated_at',
    ];
}
