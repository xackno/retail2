<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class articulos extends Model
{
    use HasFactory;
    
     protected $fillable = [
      'id',
      'clave',
      'descripcion_articulo',
      'unidad',
      'precio_compra',
      'precio_venta',
      'mayoreo_1',
      'mayoreo_2',
      'mayoreo_3',
      'mayoreo_4',
      'codigo_sat',
      'proveedor',
      'categoria',
      'linea',
      'marca',
      'existencia',
      'local',
      'ubicacion_producto',
      'fotos',
      'palabra_clave',
      'url',
      'codigo_barra',
      'caducidad',
      'descripcion_catalogo',
      'created_at',
      'updated_at'
    ];
}
