<?php

namespace App\Http\Controllers;

use App\models\articulos;
use App\models\turnos;
use App\models\ventas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cajas;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;

class ventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dia   = date("N");
        $mes   = date("m");
        $dias  = ["Lun", "Mar", "Mie", "Juv", "Vie", "Sab", "Dom"];
        $meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
        // $fecha = $dias[$dia - 1] . "  " . date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $fecha = date("d") . " " . $meses[$mes - 1];

        $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->orderBy("id", "DESC")
            ->get();
        if(isset($turno[0])){
            $turno=$turno[0]->id;    
        }else{$turno=0;}

        $mac=$this->GetMAC();

        $caja=Cajas::where("mac","=",$mac)->first();

        if ($caja!=null) {
            $mimac=$caja->mac;
        }else{
            $mimac="";
        }

        if($mimac==$mac){
            return view('forms.ventas', compact("turno", "fecha","caja"));
        }else{
            $msj="No esposible ir a la vista de VENTA. Es necesario agregar su equipo como una caja.";
            return back()->with("msj",$msj);
        }   
    }
    function GetMAC(){
        ob_start();
        system('getmac');
        $Content = ob_get_contents();
        ob_clean();
        return substr($Content, strpos($Content,'\\')-20, 17);
    }


    public function informes_ventas(){
        $ventas=ventas::all();
        return view("forms.list_ventas",compact('ventas'));
    }










    public function buscar_venta(Request $data)
    {
        $id_venta = $data->get("id_venta");
        $venta    = ventas::find($id_venta);
        $fecha    = $venta->created_at;
        $fecha    = date("Y-m-d H:i:s", strtotime($fecha));

        $id_pro   = $venta->id_productos;
        $id_pro   = explode(",", $id_pro);
        $cantidad = $venta->cantidad_pro;
        $cantidad = explode(",", $cantidad);
        $precio = $venta->precio_vendido;
        $precio = explode(",", $precio);

        $unidad=[];
        $nombres=[];
        for($x=0;$x<count($id_pro);$x++){
            $articulo=articulos::find($id_pro[$x]);
            array_push($unidad,$articulo->unidad);
            array_push($nombres,$articulo->descripcion_articulo);
        }
        return compact('venta', "fecha",'id_pro','cantidad','precio', 'unidad','nombres');
    }
    public function devolucion(Request $data)
    {
        $id_venta    = $data->get("id_venta");
        $total_venta = $data->get("total_venta");
        $id_p        = $data->get("ip_p");
        $cantidad    = $data->get("cantidad");

        //buscar la venta
        $venta = ventas::find($id_venta);
        try {
            $venta->update([
                "efectivo"     => "devolucion",
                "total_venta"  => $total_venta,
                "cantidad_pro" => implode(",", $cantidad),
                "id_productos" => implode(",", $id_p),
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function ejecutar_venta(Request $data)
    {
        date_default_timezone_set('america/mexico_city');
        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $subtotal    = $data->get("subtotal");
        $total       = $data->get("total");
        $efectivo    = $data->get("efectivo");
        $user        = $data->get("user");
        $id_turno    = $data->get("id_turno");

        $cambio=$efectivo-$total;
        //obteniendo mac de la caja
        $mac=$this->GetMAC();
        $caja=Cajas::where("mac","=",$mac)->first();
        $mimac=$caja->mac;
        if (true) {//$mimac==$mac
            
            try {
                \DB::beginTransaction();
                $venta = new ventas();
                $venta->fill([
                    "usuario"      => $user,
                    "id_turno"     => $id_turno,
                    "caja"         => $caja->id,
                    "efectivo"     => $efectivo,
                    "total_venta"  => array_sum($subtotal),
                    "cantidad_pro" => implode(",", $cantidad),
                    "id_productos" => implode(",", $id_p),
                    "precio_vendido"=>implode(",",$precio),
                ]);
                $venta->push();
                \DB::commit();
                $idv=$venta->id;
                
                $x=0;
                foreach($id_p as $id){//restando existencia
                   $articulo=articulos::find($id);
                   $articulo->update(["cantidad"=>$articulo->cantidad-$cantidad[$x]]);
                   $x++;
                }

            ////___________IMPRIMIR TICKET####################################################################
            if($data->get("ticket")=="true"){
               $this->ticketventa($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal); 
               $status="  VENTA CORRECTA CON TICKET  #".str_pad($idv, 8, '0', STR_PAD_LEFT);
           }else{
            $status="  VENTA CORRECTA  #".str_pad($idv, 8, '0', STR_PAD_LEFT);
           }
            //$this->ticket_for_client_printer($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal);
        } catch (Throwable $e) {
            \DB::rollback();
            $status = "ERROR al realizar la venta";
        }
           
        }else{
            $status="No es posible ir a la vista de VENTA. Es necesario agregar su equipo como una caja.";
        }
        return json_encode($status);
    }

    function ticketventa($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal){
        $nombreImpresora = "XP-58";//POS-58
        $connector = new WindowsPrintConnector($nombreImpresora);
        $im = new Printer($connector);
        $im->setJustification(Printer::JUSTIFY_CENTER);
        // $im->setTextSize(1, 1);
        $idventa=str_pad($idv, 8, '0', STR_PAD_LEFT);
        $im->text("VENTA:#".$idventa."\n Atendio:".Auth::user()->name." Fecha:".date("d-m-Y")."\n");
        $im->text("|DESC |CANT| UNID | $ | TOTAL\n_________________________________");
        for($x=0;$x< count($id_p);$x++){
        $im->text($descripcion[$x]."\n".$cantidad[$x]." ".$unidad[$x]." X ".$precio[$x]."=".$subtotal[$x]);    
        }
        $im->setJustification(Printer::JUSTIFY_RIGHT);
        $total=array_sum($subtotal);
        $im->text("\npago:$".number_format($efectivo,2, '.', '')."\n TOTAL:$".number_format($total,2, '.', '')."\n cambio:$".number_format(($efectivo-$total),2, '.', ''));
        
        $im->feed(5);
        $im->setJustification(Printer::JUSTIFY_LEFT);
        // $im->close();
    }

    function ticket_for_client_printer($user,$idv,$efectivo,$id_p,$cantidad,$unidad,$descripcion,$precio,$subtotal){//guarda la informacion de la venta en el archivo imprimir.json 
        $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $ticket_json=array("proceso"=>$proceso,"tipo"=>"venta",'cajero'=>$user,"fecha"=>date("d-m-Y"),'venta'=>$idv,"total"=>array_sum($subtotal),"efectivo"=>$efectivo,
        'id_art'=>$id_p,'cantidad'=>$cantidad,'unidad'=>$unidad,'descripcion'=>$descripcion,'precio'=>$precio,'importe'=>$subtotal);
        $json_venta=json_encode($ticket_json);
        \Storage::disk('files')->put("imprimir.json",$json_venta);
    }

    public function cotizacion(Request $data)
    {

        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $subtotal    = $data->get("subtotal");

        $id_p        = explode(",", $id_p);
        $cantidad    = explode(",", $cantidad);
        $unidad      = explode(",", $unidad);
        $precio      = explode(",", $precio);
        $subtotal    = explode(",", $subtotal);
        $descripcion = explode("=>", $descripcion);

        // return view('plantillas.cotizacion', compact('id_p','cantidad','unidad','descripcion','precio','subtotal'));

        $view = \View::make('plantillas.cotizacion', compact('id_p', 'cantidad', 'unidad', 'descripcion', 'precio', 'subtotal'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Formato salida-' . date('d-m-Y'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
