<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\clientes;
class clientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$title="CLIENTES y RUTAS";
        $clientes=clientes::paginate(10);
        return view('forms.clientes',compact('title','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try {
            $cliente=clientes::create($request->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        return json_encode($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $data)
    {

    }
    public function showCliente(Request $data)
    {
        $id=$data->get("id");
        $cliente=clientes::where("id_Cliente","=",$id)->get();
        // printf($cliente);
            return json_encode($cliente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->get("id_cliente");
        try {
            $cliente=clientes::where("id_cliente","=",$id);
            $cliente->update($request->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";   
        }
        return json_encode($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $data)
    {
        $id=$data->get('id');
        try {
            $cliente=clientes::where("id_cliente","=",$id);
            $cliente->delete();
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        return json_encode($status);
    }
}
