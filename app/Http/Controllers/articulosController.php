<?php

namespace App\Http\Controllers;

use App\Models\articulos;
use App\Models\marcas;
use App\Models\provedores;
use App\Models\Categorias;
use App\Models\Lineas;
use Illuminate\Http\Request;

class articulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title="ARTICULOS";
        $busqueda = "";
        if ($request->get("busqueda")) {
            $busqueda = $request->get("busqueda");
        }
        # Exista o no exista búsqueda, los ordenamos
        $articulos = articulos::orderBy("descripcion_articulo","ASC")->paginate(10);

        if ($busqueda) {
            # Si hay búsqueda, agregamos el filtro
            $articulos=articulos::where("descripcion_articulo", "LIKE", "%$busqueda%")
            ->orWhere("palabra_clave", "LIKE", "$busqueda")
            ->orWhere("clave", "LIKE", "$busqueda")
            ->orWhere("codigo_barra", "LIKE","$busqueda")
            ->orderBy("descripcion_articulo","ASC")
            ->paginate(10);
        }

        // $articulos = $builder->orderBy("id","ASC")->paginate(10);
        $provedores=provedores::all();
        $categorias=Categorias::all();
        $lineas=Lineas::all();
        $marcas=marcas::all();

        return view("forms.articulos",compact('title','articulos','provedores','categorias','lineas','marcas','busqueda'));
    }



    public function listar_articulos(){
        $articulos=articulos::all();
        echo json_encode($articulos);
    }


    
    public function buscarExistencia(Request $data)
    {
        $buscar    = $data->get('buscar');
        $articulos = articulos::orWhere("descripcion_articulo", "LIKE", "%$buscar%")
            ->orWhere("palabra_clave", "=", "$buscar")
            ->orWhere("clave", "=", "%$buscar%")
            ->get();
        return json_encode($articulos);
    }

    public function buscar_x_codigo(Request $data)
    {
        $buscar    = $data->get('codigo');
        $articulos = articulos::orWhere("codigo_barra", "=", "$buscar")
            ->get();
        return json_encode($articulos);
    }

    public function buscarArticulos(Request $data)
    {
        $buscar = $data->get('input');

        $articulos = articulos::orWhere("descripcion", "LIKE", "%$buscar%")
            ->orWhere("codigo", "=", "$buscar")
            ->orWhere("clave", "=", "$buscar")
            ->orWhere("cod_barra", "=", "$buscar")
            ->get();
        $marcas     = marcas::all();
        $provedores = provedores::all();
        return view("forms.articulos", compact('articulos', 'marcas', 'provedores'));
    }

    public function updateExistenca(Request $data)
    {
        $id       = $data->get('id');
        $cantidad = $data->get('cantidad');
        try {
            for ($y = 0; $y < count($id); $y++) {
                $articulo = articulos::find($id[$y]);
                $cant     = $articulo->existencia + $cantidad[$y];
                $articulo->update([
                    "existencia" => $cant,
                ]);
            }
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }
    public function eliminarArticulo(Request $data)
    {
        try {
            $articulo = articulos::find($data->get('id'));
            $articulo->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return back();
    }
    public function buscar_para_editar(Request $data)
    {
        $id       = $data->get("id");
        $articulo = articulos::find($id);
        return json_encode($articulo);
    }
    public function update_articulo(Request $data)
    {
        $id_p = $data->get("id_p");
        try {
            $articulo = articulos::find($id_p);
            $articulo->update($data->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $articulos = articulos::create(
                [
                    'clave'=>$request->get('clave'),
                    'unidad'=>$request->get('unidad'),
                    'descripcion_articulo'=>$request->get('descripcion_articulo'),
                    'precio_compra'=>$request->get('precio_compra'),
                    'precio_venta'=>$request->get('precio_venta'),
                    'mayoreo_1'=>$request->get('mayoreo_1'),
                    'mayoreo_2'=>$request->get('mayoreo_2'),
                    'mayoreo_3'=>$request->get('mayoreo_3'),
                    'mayoreo_4'=>$request->get('mayoreo_4'),
                    'codigo_sat'=>$request->get('codigo_sat'),
                    'proveedor'=>$request->get('proveedor'),
                    'categoria'=>$request->get('categoria'),
                    'linea'=>$request->get('linea'),
                    'marca'=>$request->get('marca'),
                    'existencia'=>$request->get('existencia'),
                    'ubicacion_producto'=>$request->get('ubicacion_producto'),
                    'fotos'=>$request->get('fotos'),
                    'palabra_clave'=>$request->get('palabra_clave'),
                    'url'=>$request->get('url'),
                    'codigo_barra'=>$request->get('codigo_barra'),
                    'caducidad'=>$request->get('caducidad'),
                    'descripcion_catalogo'=>$request->get('descripcion_catalogo'),
                ]);
            $status    = "success";
        } catch (\PDOException $e) {
           if ($e->getCode()== 23505) {
                $status="ERROR, \"Codigo de Barras \" duplicados"; 
             }else{
                // $status="ERROR, error al registrar este producto, verifique la consistencia de los datos proporcionado.";
             }
             echo $e;
        }

        return json_encode($status);
    }


     public function uploadImg(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        $nombre_articulo=$request->get("nombre");
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();

            \Storage::disk('articulos')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }


    public function store_marca(Request $request)
    {
        try {
            $marca  = marcas::create($request->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function update_marca(Request $request)
    {
        $id          = $request->get("id_marca");
        $nombre      = $request->get("nombre");
        $descripcion = $request->get("descripcion");
        $provedor    = $request->get("provedor");
        try {
            $marca = marcas::find($id);
            $marca->update([
                "nombre"      => $nombre,
                "descripcion" => $descripcion,
                "provedor"    => $provedor,
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function delete_marca(Request $request)
    {
        $id = $request->get("id");
        try {
            $marca = marcas::find($id);
            $marca->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }

    public function update_provedor(Request $request)
    {
        $id          = $request->get("id_provedor");
        $nombre      = $request->get("nombre");
        $descripcion = $request->get("descripcion");
        $rfc         = $request->get("rfc");
        $domicilio   = $request->get("domicilio");
        $tel         = $request->get("tel");
        $email       = $request->get("email");
        try {
            $marca = provedores::find($id);
            $marca->update([
                "nombre"      => $nombre,
                "descripcion" => $descripcion,
                "rfc"         => $rfc,
                "domicilio"   => $domicilio,
                "tel"         => $tel,
                "email"       => $email,
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function delete_provedor(Request $request)
    {
        $id = $request->get("id");
        try {
            $provedor = provedores::find($id);
            $provedor->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }

    public function store_provedor(Request $request)
    {
        try {
            $marca  = provedores::create($request->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }



    public function createCategoria(Request $request)
    {
       if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            categorias::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createLinea(Request $request)
    {
            if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            lineas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createMarca(Request $request)
    {
            
        if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            marcas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }











    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
