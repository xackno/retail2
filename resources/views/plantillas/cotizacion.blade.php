<!DOCTYPE html>
<html>
<head>
	<title>cotizacion</title>
	  <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
	  <link rel="stylesheet" href="{{asset('/css/atlantis.min.css')}}">
	  <link rel="stylesheet" href=" {{asset('/css/demo.css')}}">
</head>
<body >
	<div class="text-center">
		<img src="{{asset('img/logo1.png')}}" style="width: 50px">
	<b style="vertical-align: center;">STEHS 4</b>
	<br>
	</div>
	
	<p style="float: left">COTIZACIÓN</p>
	<p style="float:right">Fecha:{{date('d-m-Y')}}</p>
	<br>
	<table class=" table-bordered table-striped">
		<thead class="table-primary">
			<tr>
				<th>Cantidad</th>
				<th>unidad</th>
				<th>Descripcion</th>
				<th>precio</th>
				<th>subtotal</th>
			</tr>
		</thead>
		<tbody>

			@for($x=0;$x < count($id_p); $x++)
			<tr>
				<td>{{$cantidad[$x]}}</td>
				<td>{{$unidad[$x]}}</td>
				<td>{{$descripcion[$x]}}</td>
				<td class="text-right">${{$precio[$x]}}</td>
				<td class="text-right">${{$subtotal[$x]}}</td>
			</tr>
			@endfor
		</tbody>
	</table>
	<br>
	<h6 class="float-right" style="text-decoration: underline;"><b>TOTAL:${{number_format(array_sum($subtotal),2)}}</b> MXN</h6>
	<br>
	<br>
	<p style="text-align:justify-all;"><b>Nota:</b> Los precios son puesto en la obra Vigencia de la Cotización 15 días a partir de la fecha de emisión.</p>
	<style type="text/css">
		table{
			width: 100%
		}
		table td{
			padding: 3px;
		}table th{padding: 3px}

	</style>

</body>
</html>


