@extends('layouts.app')

@section('content')
<br><br><br>
<div class="card">
    <div class="card-header bg-info text-white">
        <h2 style="width: 100%;">USUARIOS DE SISTEMA 
            <a class="float-right btn btn-primary" href="{{url('/registrar')}}">
                <i class="fas fa-plus"></i> usuario
            </a>
            </h2>
    </div>

    <div class="card-body">
        <div class="table-responsiv" style="width: 60%;margin:auto">
            <table class="table table-bordered table-striped">
                <thead class="bg-success text-white">
                    <tr>
                        <th>nombre</th>
                        <th>usuario</th>
                        <th>caja</th>
                        <th>e-mail</th>
                        <th>tipo</th>
                        <th><i class="fas fa-cog"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user )
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->user}}</td>
                            <td>{{$user->caja}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->type}}</td>
                            <td>@if(Auth::user()->id != $user->id)
                                <button class="btn btn-success btn-sm" onclick='editar({{$user->id}},"{{$user->name}}","{{$user->user}}","{{$user->caja}}","{{$user->email}}","{{$user->type}}");'><i class="fas fa-edit"></i></button>
                                @else
                                <span class="text-danger">Esta logueado</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style type="text/css">
.table tr th{height: 30px}
.table tr td{height: 30px}
</style>




<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-info">
          <h4 id="nivel_cliente"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{url('/actualizar_user')}}">
                @csrf
                <input type="hidden" name="id">
                <label>Nombre</label>
                <input type="name" name="name" class="form-control">

                <label>Usuario</label>
                <input type="user" name="user" class="form-control">

                <label>Caja</label>
                <select name="caja" class="form-control">
                    @foreach($cajas as $caja)
                    <option value="{{$caja->id}}">{{$caja->nombre}}</option>
                    @endforeach
                </select>

                <label>E-mail</label>
                <input type="email" name="email" class="form-control">

                <label>Tipo</label>
                <select class="form-control" name="type">
                    <option value="administrador">Administrador</option>
                    <option value="vendedor">Vendedor</option>
                </select>


                <br><br>
                <button class="btn btn-success float-right"><i class="fas fa-user"></i> Actualizar</button>
            </form>
        </div>
      </div>
    </div>
  </div>







@endsection
@section('script')
<script type="text/javascript">
    function editar(id,name,user,caja,email,type) {
        // alert(type);
        $("#modal_editar").modal("show");
        $("[name=id]").val(id);
        $("[name=name]").val(name);
        $("[name=user]").val(user);
        $("[name=caja]").val(caja);
        $("[name=email]").val(email);
        $("[name=type]").val(type);
    }
</script>
@endsection