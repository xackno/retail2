<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RETAIL 2</title>
    <link rel="shortcut icon" href="{{asset('img/logo1.png')}}">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
</head>


<style type="text/css">
    body {
        font-family: 'Comfortaa', serif;
        /*font-size: 16px;*/
    } 
    .bg-gradient {
        background-image: linear-gradient(15deg, #1E90FF 0%, #00BFFF  100%);
    }
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-gradient shadow-sm">
            <div class="container">
                <a class="navbar-brand" @guest href="{{ url('/') }} @else href="{{ url('/home') }} @endguest">
                   <img src="{{asset('img/logo1.png')}}" style="width:20px">RETAIL 2
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-user"></i> Entrar</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <!-- <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> -->
                                </li>
                            @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   <i class="fas fa-user-circle"></i> {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fas fa-power-off"></i>{{ __('Salir') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-2">
            <div class=" text-center" >
                <img src="{{asset('img/logo1.png')}}" style="width: 250px;">
                <div class="motto text-center">
                    <h1>RETAIL 2</h1>
                    <h3 class="text-secondary"></h3>
                    <br>
                    <button class="btn btn-primary"><i class="fas fa-book"></i> CATÁLOGO</button>
                    <h4><!-- Sistema de punto de venta --></h4>
                    <p> </p>
                </div>

   
            </div>
            <div class="footer text-center text-secondary">
                <p> copyright &copy stehs, {{date('Y')}}.</p>
            </div>

            <style type="text/css">
                .footer{
                    width: 100%;
                    position: absolute;
                    bottom: 0px;
                }
                #buttons{
                    margin-top: 15vh;
                }
                .circulo{
                    z-index: -1;
                    height:100px;
                    width:100px;
                    border-radius: 100%;
                    padding-top: 28px !important;
                    vertical-align: text-bottom !important;
                }
                .circulo:hover{
                    height: 110px;
                    width: 110px;
                    font-size: 18px;
                    padding-top: 28px !important;
                    vertical-align: text-bottom !important;
                }
            </style>

        </main>
    </div>
</body>
</html>
<script type="text/javascript" src="{{asset('js/jquery.3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
