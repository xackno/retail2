@extends('layouts.app')

@section('content')

<!-- ###############################card 1 muestra tabla de articulso########################### -->
	<div class="container-fluid" id="card1" style="margin-top:10px">


		@if(session('success'))
					<div class="alert alert-success alert-dismissible fade show">
						<h3>{{session('success')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if(session('error'))
					<div class="alert alert-danger alert-dismissible fade show">
						<h3>{{session('error')}}</h3>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif



		<div class="row">
				<div class="col-12 col-md-5 col-sm-5 col-lg-5">
					<h2 class="text-dark float-left">Articulos <i class="fas fa-box-open"></i></h2>
				</div>
				<div class="col-12 col-md-7 col-sm-7 col-lg-7 col-xl-7">
					<div  style="margin:auto" id="botones_superiores">
						<button style="margin-left: 25px" class=" btn-sm float-right btn btn-secondary" id="btn_a_card2"><i class="fas fa-arrow-right"></i></button>
						<button class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#modal_marcas"> <i class="fas fa-plus"></i>  Marca</button>
						<button class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#modal_lineas"> <i class="fa fa-tags"></i>  Linea</button>
						<button class="btn btn-sm btn-danger float-right" data-toggle="modal" data-target="#modal_categorias"> <i class="fas fa-plus"></i>  Categoria</button>
						<button class="btn btn-sm btn-warning float-right" data-toggle="modal" data-target="#modal_agregar_provedor"> <i class="fas fa-plus"></i>  Provedor</button>

						@if(count($provedores)!=0 )
						@if(Auth::User()->type=='superadmin')
						<button class="btn btn-sm btn-success float-right" id="btn_open_existencia"> <i class="fas fa-angle-double-up"></i> Existencia</button>
						@endif
						<button class="btn btn-sm btn-primary float-right" id="btn_open_modal_producto"> <i class="fas fa-plus"></i> Articulos</button>
						@endif

					</div>

				</div>
			</div>

			@if(count($provedores)==0)
				<div class="alert alert-danger">Es necesario registrar un <strong>provedor</strong> para poder registrar un articulo</div>
			@endif

			<div id="alertas">
			</div>
			<div class="row">
				<div class="col-12 col-md-3 col-lg-3 col-xl-3"></div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3"></div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					@if($busqueda!='' || $busqueda!=null)
					<a href="{{url('/articulos')}}" class="btn btn-warning btn-sm float-right"><i class="fas fa-eye"></i> Todos</a>
					<br><br>
				@endif
				</div>
				<div class="col-12 col-md-3 col-lg-3 col-xl-3">
					<form  action="{{url('/articulos')}}" method="get">
						<div class="input-group">
							<input type="text" name="busqueda" class="form-control" placeholder="Buscar..." autofocus="true">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-search"></i></button>
						</div>
					</form>	
				</div>
			</div>
			<br>

			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tabla_articulos">
					<thead class="bg-primary text-white">
					<tr class="text-center">
						<th>Clave</th>
						<th>Unidad</th>
						<th>Descripción</th>
						<th>Precios</th>
						<th>Cant</th>
						<th><i class="fa fa-truck text-warning"></i></th>
		        <th><i class="fas fa-code-branch text-danger"></i></th>
		        <th><i class="fa fa-tags text-info"></i></th>
		        <th><i class="fa fa-map-marker text-white"></i></th>
		        <th><i class="fas fa-images text-white"></i></th>
						<th><i class="fas fa-barcode fa-2x"></i></th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
					<tbody>
						@foreach($articulos as $r)
						<tr>
							<td class="text-right">{{$r->clave}}</td>
							<td>{{$r->unidad}}</td>
							<td>
								<b class="text-primary">{{$r->descripcion_articulo}}</b>
							</td>
							<td>
								<select class="form-control">
									@if(Auth::User()->type=='superadmin')
										<option value="{{$r->precio_compra}}">Compra $ {{$r->precio_compra}}</option>
									@endif
									<option value="{{$r->precio_venta}}">Venta $ {{$r->precio_venta}}</option>
									<option value="{{$r->mayoreo_1}}">May1 $ {{$r->mayoreo_1}}</option>
									<option value="{{$r->mayoreo_2}}">May2 $ {{$r->mayoreo_2}}</option>
									<option value="{{$r->mayoreo_3}}">May3 $ {{$r->mayoreo_3}}</option>
									<option value="{{$r->mayoreo_4}}">May4 $ {{$r->mayoreo_4}}</option>
								</select>
							</td>
							
							<td class="text-right"><b class="text-danger">{{$r->existencia}}</b> </td>
							<td></td>
							<td></td>
							<td></td>
							<td>{{$r->ubicacion_producto}}</td>
							<td class="td_imagen">
								<span class="d-none">{{$r->fotos}}</span>	
								@php	
									$cont=0;
									$fotos='';
								@endphp
								@if($r->fotos!="ninguno")
									@php $fotos=explode(",",$r->fotos); @endphp
								@else
									@php
										$fotos='';
									@endphp
								@endif
								@if($fotos!=null || $fotos!='' )
									@foreach($fotos as $f)
										@if($cont<3)
											<img src="{{asset('/img/articulos/'.$f)}}" width="20px">
										@endif
										@php
											$cont++;
										@endphp
									@endforeach
								@endif
							</td>
							<td class="text-center">{{$r->codigo_barra}}</td>
							<td>
								<button class="btn btn-info btn-sm "  onclick="editar({{$r->id}});"  @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
									<i class="fas fa-edit "></i></button>

								<button class="btn btn-danger btn-sm " onclick="eliminar({{$r->id}});" @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
									<i class="fas fa-trash fa-sm"></i>
								</button>

							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="float-right">
					@if(Request::path()=='articulos')
						{{$articulos->links()}}
					@endif
				</div>
			</div>
			<style type="text/css">
				#botones_superiores button{
					margin-right: 5px;
				}
					.table th{
						padding:0px !important;
						height:25px !important;
					}
					.table td{
						height: 25px !important;
						padding: 3px !important;
					}
					#tabla_articulos_filter{ text-align: right; }
					#tabla_marcas_filter{text-align: right; }
					#tabla_provedores_filter{text-align: right; }
					#tabla_articulos tbody tr:hover{
						background: #D6EAF8 !important;
					}
			</style>
	</div>
<!-- #############CARD 2 LISTAS DE MARCAS Y PROVEDORES########################################## -->
	<div class="container-fluid" id="card2">
			<div class="row">
				<div class="col">
					<h2 class="text-white float-left">Tablas <i class="fas fa-table"></i></h2>
				</div>
				<div class="col">
					<button class="float-right btn btn-secondary" id="btn_a_card1"><i class="fas fa-arrow-left"></i></button>
				</div>
			</div>

			<div id="alertas_card2">
			</div>
			<div class="row">
				<div class="col-12 col-lg-12 col-md-12 col-xl-12">
					<h3 class="text-center text-primary">Tabla provedores</h3>
					<div class="table-responsive">
						<table class="table table-striped table-bordered" id="tabla_provedores">
							<thead class="bg-warning ">
								<tr>
									<th>#</th>
									<th>nombre</th>
									<th>descripción</th>
									<th>Domicicio</th>
									<th>Teléfono</th>
									<th>E-mail</th>
									<th>RFC</th>
									<th><i class="fas fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								@foreach($provedores as $provedor)
									<tr>
										<td>{{$provedor->id}}</td>
										<td>{{$nombre_pro=$provedor->nombre}}</td>
										<td>{{$descri_pro=$provedor->descripcion}}</td>
										<td>{{$domicilio_pro=$provedor->domicilio}}</td>
										<td>{{$tel_pro=$provedor->tel}}</td>
										<td>{{$email_pro=$provedor->email}}</td>
										<td>{{$rfc_pro=$provedor->rfc}}</td>
										<td style="padding: 0px;width:50px;padding: 0 1px !important;">
											<div class="input-group">
											<button class="btn btn-sm btn-success" style="margin-right: 20px" onclick="editar_provedor({{$provedor->id}},'{{$nombre_pro}}','{{$descri_pro}}','{{$domicilio_pro}}','{{$tel_pro}}','{{$email_pro}}','{{$rfc_pro}}' );"  @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
												<i class="fas fa-edit"></i>
											</button>

											<button class="btn btn-sm btn-danger" onclick="eliminar_provedor({{$provedor->id}});"  @if(Auth::user()->type!='superadmin') disabled='' title='No es administrador' @endif>
												<i class="fas fa-trash"></i>
											</button>
										</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
<!-- ############################################################################# -->
<!-- ############################################################################# -->









<!--window modal ######modal busqueda par a existencias################-->
  <div class="modal fullscreen-modal fade" id="modal_existencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Existencia</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="alertasModal"></div>

        	<div class="input-group col-4">
        		<input type="text" id="input_buscar" class="form-control" placeholder="Buscar" autocomplete="off">
        		<button  id="btn_buscar_para_existencia" class="btn  btn-primary"> <i class="fas fa-search"></i> </button>
        	</div>
        	<br>
        	<div class="table-responsive" >
        		<table class="table table-striped table-bordered">
        			<thead class="table-success">
        				<tr>
        					<th>Entradas</th>
        					<th>Existencia</th>
        					<th>Clave</th>
        					<th >Descripción</th>
        					<th>Cod. barra</th>
        				</tr>
        			</thead>
        			<tbody id="tbody_existencia"></tbody>
        		</table>
        	</div>

        	<button class="btn btn-primary float-right" id="guardar_cambios"><i class="fas fa-save"></i> Guardar cambios</button>

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal imagenes################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;" >

      		<h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal provedor################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_provedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Provedor</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_provedor">
        		<div class="row">
        			<div class="col">
        				<input type="hidden" name="id_provedor">
        				<label>Nombre</label>
			        	<input type="text" name="nombre" class="form-control" placeholder="Nombre">

			        	<label>Descripción</label>
			        	<input type="text" name="descripcion" class="form-control" placeholder="Descripción">

			        	<label>RFC</label>
			        	<input type="text" name="rfc" class="form-control">
        			</div>
        			<div class="col">
        				<label>Domicilio</label>
			        	<input type="text" name="domicilio" class="form-control">

			        	<label>Telefono</label>
			        	<input type="number" name="tel" class="form-control">

			        	<label>Email</label>
			        	<input type="email" name="email" class="form-control">
        			</div>
        		</div>

	        	<br><br>
	        	<button type="button" id="btn_store_provedor" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
	        	<button type="button" id="btn_update_provedor" style="display: none" class="btn btn-secondary float-right"><i class="fas fa-save"></i> Actualizar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal categoria################-->
  <div class="modal fullscreen-modal fade" id="modal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #dc3545;" >
      		<h1><i class="fa fa-plus"></i> Categoria <i class="app-menu__icon  fa fa-code-fork"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeCategoria')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal linea################-->
  <div class="modal fullscreen-modal fade" id="modal_lineas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #17a2b8;" >
      		<h1><i class="fa fa-plus"></i> Linea <i class="app-menu__icon  fa        fa-tags"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeLinea')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light bg-primary" >
      		<h1><i class="fa fa-plus"></i> Marca <i class="app-menu__icon  fab fa-google-wallet"></i> </h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeMarca')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>

        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>


<!--window modal ######modal agregar productos################-->
  <div class="modal fullscreen-modal fade" id="modalagregarproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding:3px">
        	<span class="text-white"><i class="fas fa-plus" id="icon_header"></i> Registrar nuevo articulo</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="div_alert"></div>
 
        	<form   id="form_articulo">
        		<div class="row">
        		<div class="col">
        			<input type="hidden" name="id_p">
        			<label>Clave</label>
        			<input type="text" name="clave" class="form-control" placeholder="Clave">

        			<label>Unidad</label>
        			<select class="form-control" name="unidad" required="">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="pieza">Pieza</option>
        				<option value="kilogramo">Kilogramo</option>
        				<option value="litro">Litro</option>
        				<option value="gramos">Gramos</option>
        				<option value="bolsa">bolsa</option>
        			</select>
        			<label>Descripción</label>
        			<input type="text" name="descripcion_articulo" class="form-control " placeholder="descripción" required="">

        			<label>Cantidad en existencia</label>
        			<input type="number" name="existencia" class="form-control " style="margin-left: 60%;width: 40%;">

        			<label>Código de barra</label>
        			<input type="number" name="codigo_barra" class="form-control">

        			<label>Código SAT</label>
        			<input type="number" name="codigo_sat" class="form-control">
        		</div>

        		<div class="col">
        			<label>Precio de compra</label>
        			<input type="number" name="precio_compra" class="form-control">

        			<label>Precio de venta</label>
        			<input type="number" name="precio_venta" class="form-control border-success" required="">
        			<label>Precio a mayoreo 1</label>
        			<input type="number" name="mayoreo_1" class="form-control border-success">
        			<label>Precio  a mayoreo 2</label>
        			<input type="number" name="mayoreo_2" class="form-control border-success">
        			<label>Precio a mayoreo 3</label>
        			<input type="number" name="mayoreo_3" class="form-control border-success">
        			<label>Precio a mayoreo 4</label>
        			<input type="number" name="mayoreo_4" class="form-control border-success">
        		</div>

        		<div class="col">
        			<label>Provedor</label>
        			<select name="provedor" class="form-control">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="0">Ninguno</option>
        				@foreach($provedores as $r)
        					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
        				@endforeach
        			</select>

        			<label>Categoria</label>
        			<select name="categoria" class="form-control">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="0">Ninguno</option>
        				@foreach($categorias as $r)
        					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
        				@endforeach
        			</select>

        			<label>Linea</label>
        			<select name="linea" class="form-control">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="0">Ninguno</option>
        				@foreach($lineas as $r)
        					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
        				@endforeach
        			</select>

        			<label>Marca</label>
        			<select name="marca" class="form-control">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="0">Ninguno</option>
        				@foreach($marcas as $r)
        					<option value="{{$r->id}}" data-nombre="{{$r->nombre}}">{{$r->nombre}}</option>
        				@endforeach
        			</select>
        		</div>
        		<div class="col">
        			<label for="ubicacion_producto">Ubicación del producto</label>
							<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%">
							<select class="col-4" id="pasillo">
								<option>Tienda</option>
								<option>Bodega</option>
								<option>Sucursal</option>
								<option>Pasillo 1</option>
								<option>Pasillo 2</option>
							</select>
							<select class="col-4" id="anden">
								<option>CAMARA</option>
								<option>Anden 1</option>
								<option>Anden 2</option>
								<option>Anden 3</option>
								<option>Anden 4</option>
							</select>
							<select class="col-3" id="vitrina">
								<option> 0</option>
								<option>Vitrina 1</option>
								<option>Vitrina 2</option>
								<option>Vitrina 3</option>
								<option>Vitrina 4</option>
							</select>
							<br>


							<label>Palabras clave</label>
							<input type="text" name="palabra_clave" class="form-control">

							<label>Caducidad</label>
							<input type="date" name="caducidad" class="form-control">

							<label>URL</label>
							<input type="text" name="url" class="form-control">

							<label>Foto</label>
							<input type="text" name="fotos" id="fotos" class="form-control" readonly>

							<label>Descripción para catálogo</label>
							<textarea name="descripcion_catalogo" class="form-control"></textarea>



        		</div>
        	</div>
        	<br><br>
        	<button type="button" class="btn btn-success float-right" id="btn_submit_actualizar" style="display: none">Actualizar</button>
        	<button type="button" id="btn_submit_articulo" class="btn btn-primary float-right"><i class="fas fa-save"></i> Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formuploadajax" method="post">
        		@csrf
        		<label for="imagen"><b>Subir imagen:</b></label>
			  		<input type="file" name="imagen" id="imagen"/>
			  		<input type="hidden" name="nombre" id="nombre_articulo">
			  		<button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>



        </div>
      </div>
    </div>
  </div>



@endsection

@section('script')
<script type="text/javascript">

//##########################PARA MODAL AGREGAR ARTICULO####################################
	var cont=0;
	var imagen="";
	$('#modalagregarproductos').on('shown.bs.modal', function () {
	    $('[name=clave]').focus();
	});

	$("[name=descripcion_articulo]").keyup(function(){
		$("#nombre_articulo").val($(this).val());
	});

//-------------------------al seleccionar ubicacion del articulo---------------------------
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});

	$("#guardarProducto").click(function(){
		cont=0;
	});

	//------------------------SUBIR IMAGEN----------------------------
		$("#formuploadajax").on("submit", function(e){
      e.preventDefault();
      var f = $(this);
      var formData = new FormData(document.getElementById("formuploadajax"));
      formData.append("dato", "valor");
      //formData.append(f.attr("name"), $(this)[0].files[0]);
      $.ajax({
          url: "{{url('/uploadImg')}}",
          type: "post",
          dataType: "",
          data: formData,
          cache: false,
          contentType: false,
		 			processData: false
		      }).done(function(e){
			// alert(e.nombre);
			if (e.status=="success") {
				if (cont==0) {
					imagen=e.nombre;
				}else{
					imagen=imagen+","+e.nombre;
				}
				cont++;
				
				$("#fotos").val(imagen);

				$("#subirImg").removeClass("btn btn-primary");
				$("#subirImg").addClass("btn btn-success");
				$("#subirImg").text("Subido");
			}else{
				$("#subirImg").removeClass("btn btn-primary");
				$("#subirImg").addClass("btn btn-danger");
				$("#subirImg").text("ERROR");
			}
		}).fail(function (jqXHR, exception) {
	    // Our error logic here
	    console.log(exception);
	    });
	});


//####################Modal ver foto en grande############
	//_______________________________click a  la imagen____________________
	$("table tbody .td_imagen").click(function(){
		// alert($(this).find("span").html());
		var imagenes=$(this).find("span").html();
		if (imagenes=='' || imagenes=='ninguno') {
			$("#modal_imagenes .modal-body").html("");
			$("#modal_imagenes .modal-body").append("<div style='width:50%;margin:auto' ><img src='{{asset('')}}"+"img/articulos/no-images.jpg' style='width:50%;height:auto;'></div>");
			$("#modal_imagenes").modal('show');
		}else if(imagenes!='' || imagenes!='ninguno'){
			imagenes=imagenes.split(",");
		$("#modal_imagenes .modal-body").html("");
		for(var x=0;x<imagenes.length;x++){
			$("#modal_imagenes .modal-body").append("<div style='width:100%;margin:auto'><img src='{{asset('')}}"+"img/articulos/"+imagenes[x]+"' style='width:50%;height:auto;'></div>");
		}
		$("#modal_imagenes").modal('show');
		}

	});









//######################script para el card 2######################
	$("#btn_update_provedor").click(function(){
		$.ajax({
			url:"{{url('/update_provedor')}}",
			type:"post",
			dataType:"json",
			data:$("#form_provedor").serialize(),
			success:function(e){
				if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Modificado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			},error:function(){
				$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
			}
		});
	});

	function editar_provedor(id,nombre,descripcion,domicilio,tel,email,rfc){
		$("#modal_agregar_provedor").modal("show");
		$("#form_provedor")[0].reset();
		$("#btn_update_provedor").show();
		$("#btn_store_provedor").hide();
		$("#form_provedor [name=id_provedor]").val(id);
		$("#form_provedor [name=nombre]").val(nombre);
		$("#form_provedor [name=descripcion]").val(descripcion);
		$("#form_provedor [name=rfc]").val(rfc);
		$("#form_provedor [name=domicilio]").val(domicilio);
		$("#form_provedor [name=tel]").val(tel);
		$("#form_provedor [name=email]").val(email);
	}
	function eliminar_provedor(id){
		msj=confirm("Desea eliminar este registro?, tenga en cuenta que hay articulos relacionada a este provedor lo cual no se mostrarán la información correcta si se elimina este registro.");
		if (msj) {
			$.ajax({
				url:"{{url('/delete_provedor')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Eliminado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}

				},error:function(){
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			});
		}
	}


	$("#card2").hide();
	$("#btn_a_card1").click(function(){
		$("#card2").hide();
		$("#card1").show();
	});
	$("#btn_a_card2").click(function(){
		$("#card1").hide();
		$("#card2").show();
	});

	// $("#tabla_articulos").DataTable({
	// 		"order": [[ 0, 'DESC' ]],
	// 		"language": {
	//         "decimal": "",
	//         "emptyTable": "No hay información",
	//         "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	//         "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	//         "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	//         "infoPostFix": "",
	//         "thousands": ",",
	//         "lengthMenu": "Mostrar _MENU_",
	//         "loadingRecords": "Cargando...",
	//         "processing": "Procesando...",
	//         "search": "Buscar:",
	//         "zeroRecords": "Sin resultados encontrados",
	//         "paginate": {
	//             "first": "Primero",
	//             "last": "Ultimo",
	//             "next": "Siguiente",
	//             "previous": "Anterior"
	//         }}});





	$("#tabla_provedores").DataTable({
			"order": [[ 0, 'DESC' ]],
			"language": {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Entradas",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }}});

//####################################################

	//############btn-actualizar#################
		$("#btn_submit_actualizar").click(function(){

			$.ajax({
				url:"{{url('update_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				success:function(e){

					$("#form_articulo")[0].reset();
					$("#alertas").append('<div class="alert alert-success">Articulo actualizado correctamente.</div>');

					setInterval(function(){
						location.reload();
					},4000);
					$("#modalagregarproductos").modal('hide');
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al actualizar el articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},5000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});

	//###################editar#################
		function editar(id){
			$("#form_articulo")[0].reset();
			$("#btn_submit_articulo").hide();
			$("#btn_submit_actualizar").show();
			$("#modalagregarproductos").modal("show");
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-edit");
			$.ajax({
				url:"{{route('buscar_para_editar')}}",
				type:"post",
				dataType:"json",
				data:{id:id},success:function(e){
					$("[name='id_p']").val(e.id);
					$("[name='clave']").val(e.clave);
					$("[name='descripcion_articulo']").val(e.descripcion_articulo);
					$("[name='unidad']").val(e.unidad);
					$("[name='precio_compra']").val(e.precio_compra);
					$("[name='precio_venta']").val(e.precio_venta);
					$("[name='mayoreo_1']").val(e.mayoreo_1);
					$("[name='mayoreo_2']").val(e.mayoreo_2);
					$("[name='mayoreo_3']").val(e.mayoreo_3);
					$("[name='mayoreo_4']").val(e.mayoreo_4);
					$("[name='codigo_sat']").val(e.codigo_sat);
					$("[name='proveedor']").val(e.proveedor);
					$("[name='categoria']").val(e.categoria);
					$("[name='linea']").val(e.linea);
					$("[name='marca']").val(e.marca);
					$("[name='existencia']").val(e.existencia);
					$("[name='ubicacion_producto']").val(e.ubicacion_producto);
					$("[name='fotos']").val(e.fotos);
					$("[name='palabra_clave']").val(e.palabra_clave);
					$("[name='url']").val(e.url);
					$("[name='codigo_barra']").val(e.codigo_barra);
					$("[name='caducidad']").val(e.caducidad);
					$("[name='descripcion_catalogo']").val(e.descripcion_catalogo);

				},error:function(e){
					alert("no se encontro!");
				}
			});

		}
	//######################eliminar articulo#################
		function eliminar(id){
			var msj=confirm("Desea eliminar este articulo?");
			if (msj) {
				$.ajax({
					url:"{{route('eliminarArticulo')}}",
					type:"post",
					data:{
						id:id
					},success:function(){
						$("#alertas").append("<div class='alert alert-warning'>Eliminado corectamente</div>");
						setInterval(function(){
							location.reload();
						},2000);
					},error:function(){
						$("#alertas").append("<div class='alert alert-danger'>Error al eliminar este articulo.</div>");
						setInterval(function(){
							$("#alertas").html('');
						},2000);
					}
				});
			}
		}




	//##########################existencia###########
		$("#btn_open_existencia").click(function(){
			$("#modal_existencia").modal("show");
		});
		$("#input_buscar").on("keyup",function(e){
			if (e.keyCode==13) {
				buscarproductos();
				$(this).val('');
			}
		});
		$("#guardar_cambios").hide();

		function buscarproductos(){
			$("#tbody_existencia").html('');
			$.ajax({
				url:"{{route('buscarExistencia')}}",
				type:"post",
				dataType:"json",
				data:{
					buscar:$("#input_buscar").val()
				},success:function(e){
					for(var x=0;x<e.length;x++){
						$("#tbody_existencia").append("<tr> <td style='width:20%'> <input type='number' class='form-control' style='width: 100%'></td><td>"+e[x].existencia+"</td><td>"+e[x].clave+" </td><td>"+e[x].descripcion_articulo+" </td><td class='d-none'>"+e[x].id+"</td> <td>"+e[x].codigo_barra+"</td></tr>");
					}
					$("#guardar_cambios").show();
				}
			});
		}
		$("#btn_buscar_para_existencia").click(function(){
			buscarproductos();
			$("#input_buscar").val('');
		});
		//---------------------------------------------
		$("#guardar_cambios").click(function(){
			var id=[]; var cantidad=[];
			$("#tbody_existencia").find("tr td:first-child").each(function(){
				// alert($(this).find('input').val());
				if(parseFloat($(this).find('input').val())){
					cantidad.push($(this).find('input').val());
					id.push($(this).siblings("td").eq(3).html());
				}
          });//fin each
			$.ajax({
				url:"{{route('updateExistenca')}}",
				type:"post",
				dataType:"json",
				data:{
					id:id,
					cantidad:cantidad
				},success:function(e){
					$("#tbody_existencia").html('');
					if (e=="success") {
						$("#alertasModal").append("<div class='alert alert-success'>Modificado correctamente.</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);
					}
				},error:function(){

						$("#alertasModal").append("<div class='alert alert-danger'>Error al modificar, verifique la información proporcionada...</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);

				}
			});
		});

	//##################submit provedor###############
		$("#btn_open_modal_provedor").click(function(){
			$("#modal_agregar_provedor").modal('show');
			$("#form_provedor")[0].reset();
			$("#btn_update_provedor").hide();
			$("#btn_store_provedor").show();
		});

		$("#btn_store_provedor").click(function(){
			$.ajax({
				url:"{{route('store_provedor')}}",
				type:"post",
				dataType:"json",
				data:$("#form_provedor").serialize(),
				success:function(e){
					$("#form_provedor")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Provedor agregado correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				}
			});
		});






    //################submit marca#################
		$("#btn_open_modal_marca").click(function(){
			$("#form_marca")[0].reset();
			$("#modal_agregar_marca").modal('show');
			$("#btn_store_marca").show();
			$("#btn_update_marca").hide();

		});



	//#####################submit articulos#################################
		$("#btn_open_modal_producto").click(function(){
			$("#modalagregarproductos").modal('show');
			$("#btn_submit_actualizar").hide();
			$("#btn_submit_articulo").show();
			
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-plus");
		});
		$("#btn_submit_articulo").click(function(){
			$.ajax({
				url:"{{route('store_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				beforeSend:function(){
					$("#form_articulo")[0].reset();
				},
				success:function(e){
					
					if(e=="success"){
						$("#form_articulo")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Articulo agregado correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							// location.reload();
						},3000);
						$("#modalagregarproductos").modal('hide');
					}else{
						$("#modalagregarproductos").modal('hide');
						$("#alertas").append('<div class="alert alert-danger">'+e+'</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
					}
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error en el servidor al agregar este articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},5000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});


</script>
@endsection
