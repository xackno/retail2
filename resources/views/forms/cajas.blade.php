@extends('layouts.app')

@section('content')
 @php
	 function GetMAC(){
	    ob_start();
	    system('getmac');
	    $Content = ob_get_contents();
	    ob_clean();
	    return substr($Content, strpos($Content,'\\')-20, 17);
	}
	$mac=GetMAC();
	@endphp

<div class="card">
	<div class="card-header bg-dark-gradient text-white">
		<span style="font-size: 150%"><i class="fas fa-calculator"></i> Administración de cajas de cobro</span>
		<button data-toggle="modal" data-target="#modal_agregar" id="btn_abrir_modal" class="btn btn-primary float-right"><i class="fas fa-plus"></i>CAJA</button>
	</div>
	<div class="card-body">
		<div id="msj">
        </div>

		<div class="" style="width: 50%;margin:auto;">
          <table  class="table table-striped table-bordered" >
               <thead class="bg-danger">
                   <tr>
                     <!--  <th >#</th> -->
                      <th>Nombre</th>
                      <th>MAC</th>
                      <th>impresora</th>
                      <th><i class="fas fa-cog"></i></th>
                   </tr>
               </thead>

               <tbody >
               	@foreach($cajas as $caja)
               		<tr>
               			<!-- <td>CAJA {{$caja->id}}</td> -->
               			<td>{{$nombre=$caja->nombre}}</td>
               			<td style="text-transform: uppercase;">{{$mac=$caja->mac}}</td>
               			<td>{{$nombre_impresora=$caja->nombre_impresora}}</td>
               			<td>
               				<button class="btn btn-sm btn-success" onclick='editar({{$caja->id}},"{{$caja->nombre}}","{{$caja->mac}}","{{$caja->marca_impresora}}","{{$caja->nombre_impresora}}","{{$caja->mm_impresora}}");'><i class="fas fa-edit"></i></button>
               				<button class="btn btn-danger btn-sm" onclick="eliminar({{$caja->id}});"><i class="fas fa-trash"></i></button>
               			</td>
               		</tr>
  				@endforeach
               </tbody>
           </table>
        </div>
	 </div>
</div>

<style type="text/css">
	.table  th {
          padding:0px;
          height: 32px;
          color:white
        }
</style>




<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header text-white bg-info">
        	<span id="titulo"></span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="msj2"></div>
        	<form id="form_caja" method="post" action="{{url('/actualizar_caja')}}">
        		@csrf
        		<label>Nombre</label>
      			<input type="text" name="nombre" placeholder="Nombre de la caja" class="form-control" required="">

      			<label>MAC de la pc</label>
      			<input type="text" name="mac" value="{{$mac}}" class="form-control" required="" style="text-transform: uppercase;">
      			<span class="text-secondary float-right"><a target="_blank" href="https://www.xataka.com/basics/que-es-la-direccion-mac-de-tu-ordenador-del-movil-o-de-cualquier-dispositivo">como obtener mi dirección MAC</a></span>
      			<br>

      			<label>Marca de la impresora de ticket</label>
      			<select name="marca_impresora" class="form-control">
      				<option value="epson">EPSON</option>
      				<option value="POS-58">Xprinter</option>
      			</select>

      			<label>Nombre de la impresora</label>
      			<input type="text" name="nombre_impresora" class="form-control" placeholder="" required="">

      			<label>mm de la impresora</label>
      			<select class="form-control" name="mm_impresora">
      				<option value="58">58mm</option>
      				<option value="70">70mm</option>
      				<option value="80">80mm</option>
      			</select>
      			<br>
      			<input type="hidden" name="id">
      			<button type="button" id="btn_guardar" class="btn btn-success float-right">Guardar <i class="fas fa-save"></i></button>
      			<button type="submit" class="btn btn-primary float-right" id="btn_actualizar" style="display: none">
      				Actualizar
      			</button>
        	</form>
      		
        </div>
      </div>
    </div>
  </div>

@endsection
@section('script')
<script type="text/javascript">

function eliminar(id){
	var msj=confirm("desea eliminar este registro?  los usuario que estan agregado a esta caja ya no podran realizar ventas asta que se le asigne a otra caja disponible.");
	if (msj) {
		$.ajax({
			url:"{{url('/eliminar_caja')}}",
			type:"post",
			dataType:"json",
			data:{id:id},
			success:function(e){
				if(e=="success"){
					$("#msj").html("<div class='alert alert-success'>Se eliminó correctamente este registro.	</div>");
					
				}

				setInterval(function(){
					$("#msj").html('');
					location.reload();
				},2000);
			},error:function(){
				$("#msj").html("<div class='alert alert-danger'>Hubo un <strong>ERROR</strong> al eliminar este registro.</div>");
				setInterval(function(){
					$("#msj").html('');
				},4000);
			}

		});
	}
	
}



function editar(id,nombre,mac,marca,nombre_impresora,mm){
// alert(id+nombre+mac+marca+nombre_impresora+mm);
$("#modal_agregar").modal("show");
$("#btn_actualizar").show();
$("#btn_guardar").hide();
$("#titulo").html("Actualizar "+nombre);
$("[name=id]").val(id);
$("[name=nombre]").val(nombre);
$("[name=mac]").val(mac);
$("[name=marca_impresora]").val(marca);
$("[name=nombre_impresora]").val(nombre_impresora);
$("[name=mm_impresora]").val(mm);


}

$("#btn_abrir_modal").click(function(){
	$("#btn_actualizar").hide();
	$("#btn_guardar").show();
	$("#titulo").html('<i class="fas fa-plus"></i> Agregar caja');
	$("#form_caja")[0].reset();
});

	$("#btn_guardar").click(function(){
		$.ajax({
			url:"{{url('/registrar_caja')}}",
			type:"post",
			dataType:"json",
			data:$("#form_caja").serialize(),
			success:function(e){
				if(e=="success"){
					$("#msj").html("<div class='alert alert-success'>Esta caja se registró satisfactoriamente. Recarge la página para ver el registro en la tabla.	</div>");
					$("#form_caja")[0].reset();
					$("#modal_agregar").modal("hide");
				}

				setInterval(function(){
					$("#msj").html('');
					location.reload();
				},4000);
			},error:function(){
				$("#msj2").html("<div class='alert alert-danger'>Hubo un <strong>ERROR</strong> al registrar esta caja, puede que ya exista una caja con la misma Dirección MAC.</div>");
				setInterval(function(){
					$("#msj").html('');
				},4000);
			}

		});
	});

</script>
@endsection