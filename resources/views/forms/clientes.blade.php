@extends('layouts.app')

@section('content')


<div class="container-fluid" style="margin-top:10px">
	
	<div class="row">
		<div class="col-10"></div>
		<div class="col-2">
			<div class="input-group float-right">
					<button class="btn btn-sm btn-warning float-right" id="btn_a_modal_cliente">
						<i class="fas fa-plus"></i> Cliente</button>
				</div>
		</div>
	</div>


</div>





















<div class="card" style="margin-top: 60px;margin-left: auto">
	<div class="card-header bg-info text-white">
		<div class="row">
			<div class="col">
				<span style="font-size: 150%"><i class="fas fa-users"></i> Clientes </span>
			</div>
			<div class="col"></div>
			<div class="col"></div>
			<div class="col">
				<div class="input-group float-right">
					<button class="btn btn-sm btn-warning float-right" id="btn_a_modal_cliente">
						<i class="fas fa-plus"></i> Cliente</button>
				</div>
			</div>
		</div>
		
	</div>
	<div class="card-body">
		<div id="alertas"></div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead class="table-primary text-center">
					<tr>
						<th>#</th>
						<th>Nombre y apellido</th>
						<th>domicilio</th>
						<th>Tel</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody class="text-center">
					@foreach($clientes as $cliente)
					<tr>
						<td>{{$cliente->id_cliente}}</td>
						<td>{{$cliente->nombre_cliente}}</td>
						<td>{{"Calle: ".$cliente->calle.", Col.: ".$cliente->colonia.", ".$cliente->poblacion.", ".$cliente->municipio}}</td>
						<td>{{$cliente->tel}}</td>
						<td>
							@if(Auth::user()->type=="administrador")
							<button onclick="editar({{$cliente->id_cliente}});" class="btn btn-sm btn-success " style="padding: 2px"><i class="fas fa-edit fa-2x"></i></button>
							<button onclick="eliminar({{$cliente->id_cliente}});"  class="btn btn-sm btn-danger " style="padding: 2px"><i class="fas fa-trash fa-2x"></i></button>
							@endif
							<button onclick="vercard({{$cliente->id_cliente}});" class="btn btn-sm btn-warning " style="padding: 3px"><i class="fas fa-address-card fa-2x"></i></button>

							<button  class="btn btn-sm btn-info " style="padding: 2px"><i class="fas fa-eye fa-2x"></i></button>

							
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{$clientes->links()}}
		</div>
		<style type="text/css">
			.table th{
				/*padding: 50px;*/
				height: 40px;
			}
		</style>
		
	</div>
</div>







<!-- ############################################################## -->
<!--window modal ######modal agregar clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus" id="icon_header"></i><i class="fas fa-user"></i> cliente</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="alertasModal"></div>
        	<form id="form_cliente">
        		<div class="row">
        			<div class="col">
        				<input type="hidden" name="id_cliente">
        				<label>Nombre completo</label>
        				<input type="text" name="nombre_cliente" class="form-control" placeholder="Nombre y apellidos" title="Nombre apellido paterno apellido materno">

        				<label>Identificación (INE o CURP)</label>
        				<input type="text" name="identificacion" class="form-control" placeholder="su INE o el CURP">

        				<label>Municipio</label>
        				<input type="text" name="municipio" class="form-control" placeholder="Municipio al cual pertenece">

        			</div>
        			<div class="col">
        				<label>Población</label>
        				<input type="text" name="poblacion" class="form-control" placeholder="nombre de la población">

        				<label>Colonia</label>
        				<input type="text" name="colonia" class="form-control" placeholder="Colonia">

        				<label>Calle</label>
        				<input type="text" name="calle" class="form-control" placeholder="Calle y número">

        			</div>
        			<div class="col">
        				<label>RFC</label>
        				<input type="text" name="rfc" class="form-control" >

        				<label>Teléfono</label>
        				<input type="number" name="tel" class="form-control" >

        				<label>E-mail</label>
        				<input type="email" name="email" class="form-control">
        			</div>
        		</div>
        		<br><br>
        		<button type="button" class="btn btn-warning float-right d-none" id="updateUser">Guardar modificación</button>
        		<button type="button" class="btn  btn-success float-right" id="btn_submit_cliente"><i class="fas fa-save"></i> Guarda</button>
        	</form>
        	

        </div>
      </div>
    </div>
  </div>

<!-- ############################################################## -->
<!--window modal ######modal agregar clientes################-->
  <div class="modal fullscreen-modal fade" id="modal-card-cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white header-card-cliente" style="font-size: 160%" >
        		
        	</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div class="text-center">
        		<i class="fas fa-user fa-3x"></i>
        		<hr>
        		<label>Nombre: <b class="card-nombre" style="text-transform: uppercase;"></b></label>
        		<label>Dirección: <b class="card-direccion text-success"></b></label>
        		<label>Teléfono: <b class="card-tel text-danger"></b></label>
        		<label>E-mail: <a href="" class="card-email"></a></label><br>
        		<label>RFC:<b class="card-rfc"></b> </label>
        	</div>

        </div>
      </div>
    </div>
  </div>


@endsection
@section('script')
<script type="text/javascript">


//###################funciones#######################

	function editar(id){
		$("#modal_cliente").modal("show");
		$("#icon_header").removeClass();
		$("#icon_header").addClass("fas fa-edit");
		$("#updateUser").removeClass("d-none");
		$("#btn_submit_cliente").addClass("d-none");
		$.ajax({
			url:"{{route('showCliente')}}",
			type:"post",
			dataType:"json",
			data:{id:id},
			success:function(e){
				$("[name='id_cliente']").val(e[0].id_cliente);
				$("[name='nombre_cliente']").val(e[0].nombre_cliente);
				$("[name='identificacion']").val(e[0].identificacion);
				$("[name='poblacion']").val(e[0].poblacion);
				$("[name='colonia']").val(e[0].colonia);
				$("[name='calle']").val(e[0].calle);
				$("[name='municipio']").val(e[0].municipio);
				$("[name='rfc']").val(e[0].rfc);
				$("[name='tel']").val(e[0].tel);
				$("[name='email']").val(e[0].email);

			},error:function(e){
				alert("Error al buscar el cliente registro no encontrado");
			}
		});
	}
	$("#updateUser").click(function(){
		$.ajax({
			url:"{{url('/updateCliente')}}",
			type:"post",
			dataType:"json",
			data:$("#form_cliente").serialize(),
			success:function(e){
				$("#form_cliente")[0].reset();
				$("#modal_cliente").modal("hide");
				$("#alertas").append('<div class="alert alert-success">Datos modificado correctamente.</div>');
				setInterval(function(){
					$("#alertas").html('');
					location.reload();
				},3000);
			},error:function(){
				alert("Error al tratar de modificar los datos de este cliente, Verifique la información proporcionada");
			}
		});
	});

	function eliminar(id){
		var msj=confirm("Desea eliminar este cliente?");
		if (msj) {
			$.ajax({
				url:"{{route('destroyCliente')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					$("#alertas").append('<div class="alert alert-success">Cliente eliminado correctamente.</div>');
					setInterval(function(){
						$("#alertas").html('');
						location.reload();
					},2000);

				},error:function(){
					$("#alertas").append('<div class="alert alert-danger">Error al eliminar este cliente.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
				}	
			});
		}

	}
	function vercard(id){
		$("#modal-card-cliente").modal("show");
		$(".header-card-cliente").html("<i class='fas fa-address-card'></i>"+ " "+ id);

		$.ajax({
			url:"{{route('showCliente')}}",
			type:"post",
			dataType:"json",
			data:{id:id},
			success:function(e){
				$(".card-nombre").html(e[0].nombre_cliente);
				$(".card-direccion").html("<br>Calle:"+e[0].calle+"<br> Colonia: "
					+e[0].colonia+"<br> Población: "+e[0].poblacion+"<br> Municipio: "
					+e[0].municipio	);
				$(".card-tel").html(e[0].tel);
				$(".card-email").html(e[0].email);
				$(".card-rfc").html(e[0].rfc);

			},error:function(){

			}
		});
	}
	function mov(id){

	}

//#######################agregar cliente##########################################
	$("#btn_a_modal_cliente").click(function(){
		$("#modal_cliente").modal("show");
		$("#icon_header").removeClass();
		$("#icon_header").addClass("fas fa-plus");

		$("#updateUser").addClass("d-none");
		$("#btn_submit_cliente").removeClass("d-none");

		$("#form_cliente")[0].reset();
	});

	$("#btn_submit_cliente").click(function(){
		$.ajax({
			url:"{{route('storeCliente')}}",
			type:"post",
			dataType:"json",
			data:$("#form_cliente").serialize()
			,success:function(e){
				$("#form_cliente")[0].reset();
				$("#alertas").append('<div class="alert alert-success">Cliente agregado correctamente.</div>');
				setInterval(function(){
					$("#alertas").html('');
					location.reload();
				},3000);
				$("#modal_cliente").modal('hide');
			},error:function(){
				alert("Error al guardar los datos de este cliente, verifique la información proporcionada.");
			}
		});
	});

</script>
@endsection