<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>STEHS4</title>
    <link rel="shortcut icon" href="{{asset('img/logo1.png')}}">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
</head>

<body class="container-fluid">

<div class="row">
    <div class="col-0 col-md-1 col-xl-1 col-lg-1 d-none d-sm-none d-md-block text-center">
        <a  class="btn btn-primary " href="{{url('/home')}}"><i class="fas fa-arrow-left"></i> <i class="fas fa-home"></i> INICIO </a>
        <hr>
        <b>FECHA:</b>
        <p>{{$fecha}} {{date('Y')}}</p>
        <b>HORA:</b>
        <p id="hora_footer"></p>
        <b>CAJERO:</b>
        <p>{{Auth::user()->name}}</p>
        <b>TURNO:</b>
        <p @if($turno==0) class="bg-danger text-white" @endif>
          @if($turno!=0)
          <span>Turno:{{$turno}}</span>
          <input type="hidden" name="turno" id="id_turno" value="{{$turno}}">
          @else
            <span>Iniciar TURNO</span>
          @endif
        </p>
        <b>CAJA:</b>
        <p> @if(isset($caja->nombre)){{($caja->nombre)}} @else 0 @endif</p>
    </div>
    <!-- ########################################TABLA VENTA############################################################### -->
    <div class="col-12 col-md-11 col-xl-11 col-lg-11">
        <div class="tabla">
            <div style="width: 97.5%; position:absolute;">
               <table class="table table-bordered" >
                <thead class="bg-primary">
                    <th class="th_1 d-none">#</th>
                    <th class="th_2">CANTIDAD</th>
                    <th class="th_3">UNIDAD</th>
                    <th class="th_4">ARTICULO</th>
                    <th class="th_5">PRECIO</th>
                    <th class="th_6">SUBTOTAL</th>
                    <th class="th_7 text-center"><i class="fas fa-cog"></i></th>
                </thead>
            </table> 
            </div>
            <div style="overflow-y: scroll;height: 92vh; width: 100%" id="div_scroll_tabla_venta">
                <table class="table table-striped table-bordered">
                    <thead class="d-non" style="margin-right:-20px;">
                        <th class="th_1 d-none">#</th>
                        <th class="th_2">CANTIDAD</th>
                        <th class="th_3">UNIDAD</th>
                        <th class="th_4">ARTICULO</th>
                        <th class="th_5">PRECIO</th>
                        <th class="th_6">SUBTOTAL</th>
                        <th class="th_7"><i class="fas fa-cog"></i></th>
                    </thead>
                    <tbody id="tabla_venta">
                        <tr  class="d-none"><td class="d-none"></td><td id="td_focus"></td><td></td><td>NO ELIMINAR</td><td></td><td></td><td></td></tr>
                    </tbody>
                </table> 
        </div><!-- End -->

        <div id="alertas"></div>

        <div  id="footer" class="bg-primary">
            <div class="row" style="padding: 5px;">
              <div class="col">
                <div class="input-group">
                  <input type="text" name="busqueda"  class="form-control" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus="true" style="color:black;font-family: 'arial black'">
                  <button class="btn btn-sm btn-success" onclick="buscarPro();"><i class="fas fa-search fa-2x" ></i></button>
                </div>
              </div>
              <div class="col">
                <button class="btn btn-sm btn-warning" id="btn_f9"><i class="fas fa-shopping-basket text-dark fa-2x"></i></button>
                 <button class="btn btn-sm btn-warning" id="btn_f8"><i class="fas fa-power-off text-dark fa-2x"></i></button>
                 <button class="btn btn-sm btn-info" id="btn_f4"><i class="fas fa-sign-out-alt fa-2x text-dark"></i></button>
                 <button class="btn btn-sm btn-info" id="btn_devolucion"><i class=" fas fa-hand-point-left fa-2x text-dark"></i></button>
                 
              </div>
              <div class="col">
                <h4 class="text-white float-right"><span>TOTAL:$</span> <span id="totalpagar">00.00</span> MXN</h4>
              </div>
            </div>
        </div>
    </div>
</div>
</body>
<style type="text/css">
    .btn-sm{font-size: 10px;}
    #tabla_venta{overflow-y:scroll;}
    #tabla_venta td{padding: 3px;height: 33px;font-family: 'Arial Black';}
    /*Ancho de las columas de la tabla venta*/
    .th_2{width: 10%;padding: 3px !important;height: 20px;}
    .th_3{width: 10%;padding: 3px !important;height: 20px;}
    .th_4{width: 40%;padding: 3px !important;height: 20px;}
    .th_5{width: 10%;padding: 3px !important;height: 20px;}
    .th_6{width: 10%;padding: 3px !important;height: 20px;}
    .th_7{width: 10%;padding: 3px !important;height: 20px;}

    #alertas{position: absolute;width: 30%; top: 50px;left: 30%}
    
    #footer{height: 8vh;}
    body {
        font-family: 'Comfortaa', serif;
        /*font-size: 16px;*/
    } 
</style>


<!-- #################################modales###################################3 -->
<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-primary" style="padding:3px">
          <h4 id="nivel_cliente">Articulos encontrados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table-responsive" id="table_responsive_busq" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th class="d-none">Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th style="width: 50%">Descripción</th>
                      <th>$ Precio</th>
                      <th>Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                  </tr>
               </tbody>
           </table>
        </div>
        </div>
      </div>
    </div>
  </div>
<!-- $$$$$$$$$$$$$$$modal cobrar###################### -->
  <div class="modal fade" id="modal_cobrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Realizar venta
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col text-center">
              <h1 class="text-success">TOTAL:$</h1>
              <h1 id="text-total" class="text-danger">00.00</h1>
            </div>
            <div class="col text-center">
              EFECTIVO:<i class="fas fa-dollar-sign"></i>
              <input type="number" id="efectivo" class="form-control border border-primary">
            </div>
          </div><br>
          <div class="text-center">
            <h3 class="text-primary">Cambio:$<span id="cambio" class="text-danger">00.00</span></h3>
          </div>

        </div>
        <div class="card-footer">
          <!-- <button class="btn btn-success btn-sm float-left" id="btn_a_credito" style="margin-right: 4px"><i class="fas fa-credit-card"></i> Credito</button> -->

          <form  action="{{url('/credito')}}" class="float-left" id="form_credito" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="credito_id_p">
            <input type="hidden" name="credito_cantidad">
            <input type="hidden" name="credito_unidad">
            <input type="hidden" name="credito_descripcion">
            <input type="hidden" name="credito_precio">
            <input type="hidden" name="credito_subtotal">
            <input type="hidden" name="turno" value="{{$turno}}">
            <button class="btn btn-success btn-sm float-left " disabled="true"  id="btn_credito"><i class="fas fa-credit-card"></i> Crédito</button>
          </form>

          <form  action="{{url('/cotizacion')}}" class="float-left" id="form_cotizacion" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="id_p">
            <input type="hidden" name="cantidad">
            <input type="hidden" name="unidad">
            <input type="hidden" name="descripcion">
            <input type="hidden" name="precio">
            <input type="hidden" name="subtotal">
            <button class="btn btn-secondary btn-sm float-left" id="btn_cotizacion"><i class="fas fa-table"></i> Cotización</button>
          </form>

          <button class="btn btn-primary btn-sm float-right" onclick="ejecutar_venta();">Realizar <i class=" fas fa-angle-right"></i></button>

        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->
<!--window modal ######modal turno################-->
  <div class="modal fullscreen-modal fade" id="modal_turno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px" id="header_turno">Turno</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <!-- #############################from para abrir turno -->
          <form  id="form-store-turno">
            @csrf
            <h4 style="width: 100%">Usuario: {{Auth::user()->name}}<span class="float-right">{{$fecha}}</span> </h4>

            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="abierto">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="inicio" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario1" ></textarea>
            <br><br>


            <button id="btn_submit_store_turno" type="button"  class="btn btn-primary btn-sm float-right">Iniciar <i class="fas fa-power-off"></i></button>
          </form>
          <!-- #####################from para cerrar turno -->
          <form action="{{url('/cerrarturno')}}" method="post" id="form-cerrar-turno">
            @csrf
            <h4>Usuario: {{Auth::user()->name}}</h4>
            <input type="hidden" name="id" @if(isset($turno)) value="{{$turno}}" @endif >
            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="cerrado">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="cierre" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario2" ></textarea>



            <br><br>
            <button id="btn_submit_cerrar_turno" type="submit" class="btn btn-danger btn-sm float-right">Cerrar <i class="fas fa-power-off"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal salidas################-->
  <div class="modal fullscreen-modal fade" id="modal_salidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px">SALIDAS</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="padding-top: 1px">
          <div id="alertasmodal_salidas"></div>
          <div class="input-group">
            <button class="btn btn-primary btn-sm" id="toggle-registrar">Registrar</button>
            <button class="btn btn-primary btn-sm" id="toggle-ver">VER</button>
          </div>
          <div id="registrar_salidas">
              <form id="form_save_salidas">
                <div class="row">
                  <div class="col">
                    <h4 style="width: 100%;padding:0;margin: 0px">Usuario: {{Auth::user()->name}}</h4>
                  </div>

                  <div class="col">
                    @if(isset($turno))
                      <span class="text-danger" style="font-family: 'Arial Black';padding: 0">Turno: {{$turno}}</span>
                      @else
                      <span class="text-warning" style="font-family: 'Arial Black'">Iniciar TURNO</span>
                    @endif
                  </div>

                  <div class="col">
                    <h4><span class="float-right">{{$fecha}}</span></h4>
                  </div>
                </div>
                <input type="hidden" name="turno" @if(isset($turno)) value="{{$turno}}" @endif >
                <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
                <label>Concepto</label>
                <input type="text" name="concepto" class="form-control border-secondary" required="">

                <label><i class="fas fa-dollar-sign"></i> Cantidad</label>
                <input type="number" name="cantidad" id="cantidad_registro_salida" class="form-control border-secondary" placeholder="0.00">
                </form>
          </div>
          <div id="listar_salidas">
            <div class="table_responsive">
              <table class="table table-bordered table-striped" id="tabla_registros_salidas">
                <thead class="table-dark text-center">
                  <tr>
                    <th class="d-none">#</th>
                    <th>concepto</th>
                    <th>cantidad</th>
                    <th><i class="fas fa-cogs"></i></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
              <form class="input-group" id="form_edit_salida">
                <input type="hidden" name="edit_id">
                <input type="text" name="edit_concepto" class="form-control">
                <input type="number" name="edit_cantidad" class="form-control">
                <button type="button" onclick="guardar_edit_salida();" class="btn btn-primary">Guardar <i class="fas fa-save"></i></button>

              </form>
            </div>
          </div>



        </div>
        <div class="modal-footer">
          <button onclick="registrar_salidas();" id="btn-registrar-salida" class="btn btn-primary btn-sm float-right">Registrar</button>
        </div>

      </div>
    </div>
  </div>
<!-- $$$$$$$$$$$$$$$modal devolucion###################### -->
  <div class="modal fade" id="modal_devolucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Devolución para la  venta <span id="id_venta"></span>
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="alert_in_devoluciones"></div>

          <div id="div_info" style="width: 100%">

          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tabla_prod_devolucion">
            <thead class="table-success">
              <tr>
                <th class="d-none">id</th>
                <th>cantidad a devolver</th>
                <th>unidad</th>
                <th>descripcion</th>
                <th>precio</th>
                <th>subtotal</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          </div>
          <h3 class="float-right text-primary">Total de venta: $<span id="total_venta"></span></h3>
          <br><br>
          <h3 class="float-right text-danger">Total a devolver: $<span id="total_devolver"></span></h3>
          <br><br><br><br>
          <button class="btn btn-primary float-right" onclick="guardar_devolucion();">Guardar devolución</button>
          <br>
          <br>
          <div class="table bg-info rounded">
            <p class="text-white"><span class="text-warning">NOTA:</span> Verificar que la mercancia no este dañada antes de aceptar la devolución.</p>
          </div>
        </div>

      </div>
    </div>
  </div>
<!-- fin modal devolucion -->
</html>







<script type="text/javascript" src="{{asset('js/jquery.3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/for-ventas.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/for-creditos.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

    $(document).ready(function(){
        cargar_tr();
    });

    var bloqueador=0;//variable public
    //-------------------------------------------------------------------------------------------
    function cargar_tr(){
        for(var x=0;x<16;x++){
            $("#tabla_venta").append("<tr><td class='d-none'></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    }

    function eliminar_ultimo_tr_vacio(){
        var ultimo=$("#tabla_venta").find("tr:last");
        var valor=ultimo.find("td").eq(0).html();
        if (valor=='') {
            $("#tabla_venta").find("tr:last").remove();
        }
    }

    function guardar_devolucion(){
      var id_venta=$("#id_venta").html();
      var total_venta=parseFloat($("#total_venta").html())-parseFloat($("#total_devolver").html());
      var id=[];
      var cantidad_devolver=[];
      $("#tabla_prod_devolucion tbody").find("tr td:first-child").each(function() {
          id.push($(this).html());
          cantidad_devolver.push(parseFloat($(this).siblings("td").eq(0).find("i").html())-parseFloat($(this).siblings("td").eq(0).find("input").val()));
        });

      // alert(cantidad_devolver);
      $.ajax({
        url:"{{url('/devolucion')}}",
        type:"post",
        dataType:"json",
        data:{
          id_venta:id_venta,
          total_venta:total_venta,
          ip_p:id,
          cantidad:cantidad_devolver
        },
        success:function(e){
          buscar_venta(id_venta);
          $("#alert_in_devoluciones").html("<div class='alert alert-success'>Devolución correctamente</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
        },error:function(){
          $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se pudo guardar la información</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
        }
      });
    }
    function limpiar_modal_devolucion(){
      $("#tabla_prod_devolucion tbody").html('');
      $("#total_venta,#total_devolver,#id_venta").html('');
    }
    function buscar_venta(id_venta){
      $("#div_info,#total_venta,#total_devolver").html("");
      $("#tabla_prod_devolucion tbody").html("")
      $.ajax({
        url:"{{url('/buscar_venta')}}",
        type:"post",
        dataType:"json",
        data:{
          id_venta:id_venta
        },success:function(e){
          console.log(e);
          if (e !=null) {//si existe la venta mostrarla
            $("#div_info").html(
              "<span class='text-primary'>Turno: "+e['venta'].id_turno+"</span>"+
              "  <span>Atendio: "+e['venta'].usuario+"</span>"+
              "<span class='float-right text-danger'>Fecha: "+e['fecha']+"</span>");
              var total_venta=0;
            for(var x=0;x<e['id_pro'].length;x++){
              var cantidad=e['cantidad'][x];
              var subtotal=parseFloat(cantidad)* parseFloat(e['precio'][x]);
              subtotal=subtotal.toFixed(2);
              $("#tabla_prod_devolucion tbody").append("<tr><td class='d-none'>"+e['id_pro'][x]+"</td> <td > <input type='number' style='width:60px' value='"+cantidad+"'>     de <i class='d-non float-right'>"+cantidad+"</i></td> <td>"+e['unidad'][x]+"</td> <td>"+e['nombres'][x]+"</td> <td>"+e['precio'][x]+"</td> <td>"+subtotal+"</td>   </tr>");
              total_venta+=parseFloat(cantidad)* parseFloat(e['precio'][x]);
            }


            $("#total_venta").html(total_venta.toFixed(2));
            $("#total_devolver").html(total_venta.toFixed(2));
          }else{
            $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
          }
        },error:function(){
          $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
        }
      });
    }
    //##################calculando cantidad a devolver###########
    $("#tabla_prod_devolucion tbody").on("keyup || change","tr td:nth-child(2)",function(){
      var cantidad_original=parseFloat($(this).find("i").html());
      var modificado=parseFloat($(this).find("input").val());
      // alert(cantidad_original+" " + modificado);
      if (modificado>cantidad_original || modificado<1) {//si es mayor a la cantidad original mandar alerta
        $(this).find("input").val(cantidad_original);
        $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No puede ser mayor ni menor a 1</div>");
          setInterval(function(){$("#alert_in_devoluciones").html('')},2000);
          //calculando e lsubtotal
          var precio=parseFloat($(this).siblings("td").eq(3).html());
          var subtotal=cantidad_original*precio;
          $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
      }else{
        var precio=parseFloat($(this).siblings("td").eq(3).html());
        var subtotal=modificado*precio;
       $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
      }
      //calculando total de $ a devolver
      total=0;
      $("#tabla_prod_devolucion tbody").find("tr td:last-child").each(function() {
          total+=parseFloat($(this).html());
        });
      $("#total_devolver").html(total.toFixed(2));
    });
    function storeturno(){
      if (bloqueador==0) {
        bloqueador=1;
        $.ajax({
          url:"{{url('/storeturno')}}",
          type:"post",
          data:$("#form-store-turno").serialize(),
          beforeSend:function(){
            bloqueador=1;
          },
          success:function(e){
            $("#form-store-turno")[0].reset();
              $("#modal_turno").modal("hide");
              location.reload();
              setInterval(bloqueador=0,1000);
            bloqueador=0;
          },error:function(e){

          }
        });
      }
    }
    $("#form_edit_salida").hide();
    function editarsalida(id,concepto,cantidad){
      $("#form_edit_salida").show();
      $("[name=edit_id]").val(id);
      $("[name=edit_concepto]").val(concepto);
      $("[name=edit_cantidad]").val(cantidad);
    }
    function guardar_edit_salida(){
      $.ajax({
        url:"{{url('/editar_salida')}}",
        type:"post",
        dataType:"json",
        data:{
          id:$("[name=edit_id]").val(),
          concepto:$("[name=edit_concepto]").val(),
          cantidad:$("[name=edit_cantidad]").val(),
        },success:function(e){
          buscarsalidas();
          $("#form_edit_salida")[0].reset();
          $("#form_edit_salida").hide();
          $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Modificado corectamente.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        },error:function(){
          $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>Error al modificar.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        }
      });
    }
    function buscarsalidas(){
      $("#tabla_registros_salidas tbody").html('');
      $.ajax({
        url:"{{url('/buscarsalidas')}}",
        type:"post",
        dataType:"json",
        data:{
          user:{{Auth::user()->id}},
          turno:$("#id_turno").val()
        },
        success:function(e){
          for(var x=0;x<e.length;x++){
            var concepto=e[x].concepto;
            $("#tabla_registros_salidas tbody").append("<tr><td class='d-none'>"+e[x].id+"</td><td>"+e[x].concepto+"</td> <td class='text-right'>"+e[x].cantidad+"</td><td><button class='btn btn-danger btn-sm' onclick='eliminarsalida("+e[x].id+");'> <i class='fas fa-trash'></i></button> <button class='btn btn-sm btn-success' onclick='editarsalida("+e[x].id+",\""+e[x].concepto+"\",\""+e[x].cantidad+"\");'><i class='fas fa-edit'></i></button></td> </tr>");
          }
        },error:function(){
          alert("Error al buscar registros");
        }
      });
    }
    function eliminarsalida(id){
      // alert(id);
      var msj=confirm("Desea eliminar este registro?");
      if (msj) {
        $.ajax({
        url:"{{url('/eliminarsalida')}}",
        type:"post",
        dataType:"json",
        data:{id:id},
        success:function(e){
          buscarsalidas();
          $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Se eliminó corectamente.</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        },error:function(){
          $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>No se pudo eliminar</div>");
          setInterval(function(){
            $("#alertasmodal_salidas").html('');
          },4000);
        }
      });
      }
    }

    //###############registrar salidas##########
        function registrar_salidas(){

          if ($("[name=concepto]").val()!="" && $("#cantidad_registro_salida").val()!="" && bloqueador==0) {
            bloqueador=1;
            $.ajax({
            url:"{{url('/storesalida')}}",
            type:"post",
            dataType:"json",
            data:$("#form_save_salidas").serialize(),
            success:function(e){
              $("#form_save_salidas")[0].reset();
              $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>"+e+"</div>");
              setInterval(function(){
                $("#alertasmodal_salidas").html('');
              },4000);
              bloqueador=0;
            },error:function(){
              $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>ERROR AL GUARDAR ESTE REGISTRO</div>");
              setInterval(function(){
                $("#alertasmodal_salidas").html('');
              },4000);
            }
          });
          }else{
            alert("Necesita llenar todos los campos.");
          }
        }
    //###########################ejecutar venta##########################
        function ejecutar_venta(){
          var id_p=[];
          var cantidad=[];
          var unidad=[];
          var descripcion=[];
          var precio=[];
          var subtotal=[];
          var cont=0;
          var total=0;
          $("#tabla_venta").find("tr td:first-child").each(function(){
            if($(this).siblings("td").eq(4).html()!=""){
                id_p.push($(this).html());
                cantidad.push($(this).siblings("td").eq(0).html());
                unidad.push($(this).siblings("td").eq(1).html());
                descripcion.push($(this).siblings("td").eq(2).html());
                precio.push($(this).siblings("td").eq(3).find("select").val());
                subtotal.push($(this).siblings("td").eq(4).html());
                total+=parseFloat($(this).siblings("td").eq(4).html());
              }
           });
          var efectivo=parseFloat($("#efectivo").val());
          var ticket=confirm("¿Desea imprimir TICKET?");
          if(id_p.length>0){
            if (bloqueador==0) {
              if(efectivo-total >=0){
                bloqueador=1;
                $.ajax({
                        url:"{{url('ejecutar_venta')}}",
                        type:"post",
                        dataType:"json",
                        data:{
                          id_p:id_p,
                          cantidad:cantidad,
                          unidad:unidad,
                          descripcion:descripcion,
                          precio:precio,
                          subtotal:subtotal,
                          total:total,
                          efectivo:$("#efectivo").val(),
                          user:"{{Auth::user()->name}}",
                          id_turno:$("#id_turno").val(),
                          ticket:ticket
                        },success:function(e){
                          // alert(e);
                          $("#alertas").html("<div class='bg-success text-white rounded'>"+e+"</div>");
                          setInterval(function(){
                            $("#alertas").html('');
                          },5000);
                          $("#modal_cobrar").modal("hide");
                          $("#efectivo").val('');
                          limpiartablaventa();
                          bloqueador=0;
                          $("#findProducto").focus();
                        },error:function(){
                          alert("No se pudo realizar la venta.");
                        }
                });
              }else{
                alert("ERROR en el cambio, no debe de ser negativo");
              }
            }else{
              alert("No es posible realizar la venta, hay otra tarea en proceso...");
            }
          }else{
            alert("No hay articulos en la tabla");
          }
        }
//#############################MOSTRAR LOS 4 PRECIOS######################################################
    function mostrar_4_precio(p1,p2,p3,p4,p5){
      var select="<select> <option value="+p1+" selected='true'>$ "+p1+" venta.</option><option value="+p2+">$ "+p2+" May 1.</option><option value="+p3+">$ "+p3+" May. 2</option><option value="+p4+" >$ "+p4+" May 3.</option><option value="+p5+" >$ "+p5+" May 4.</option> </select>";
      return select;
    }
//####################funcion buscar por codigo de barras#############
    function buscar_x_codigo(codigo){

      $.ajax({
        url:"{{url('/buscar_x_codigo')}}",
        type:"post",
        dataType:"json",
        data:{codigo:codigo},
        success:function(e){
          if(e.length==0){
              alert("No se encontro el articulo.");
              $("#findProducto").val('');
              $("#findProducto").focus();
            }else{
              if (ya_existe(e[0].id)==0) {
                append_tabla_venta(e[0].id,e[0].unidad,e[0].descripcion_articulo,mostrar_4_precio(e[0].precio_venta,e[0].mayoreo_1,e[0].mayoreo_2,e[0].mayoreo_3,e[0].mayoreo_4),e[0].precio_venta);
                $("#findProducto").val('');
                $("#findProducto").focus();
              }
            }  
        },error:function(){
          alert("No se encontro el articulo.");
          $("#findProducto").val('');
          $("#findProducto").focus();
        }
      });
    }
    //################FUNCION DE BUSCAR PRODUCTO############################
      function buscarPro(){
        limpiartablabusqueda();
        var producto=$("#findProducto").val();
          if(producto.length>3){
            $('#modal_busqueda').modal('show');
              $.ajax({
                url:'{{route("buscarExistencia")}}',
                type:'post',
                dataType:'json',
                data:{
                  buscar:producto
                },success:function(e){
                   if (e=="") {
                     $("#tabla_busqueda tbody").append("<tr  class='text-danger'> <td  colspan='6'><b>No se encontró registro!!</b></td></tr>");
                    }else{
                      for(var x=0;x<e.length;x++){
                        var precio=mostrar_4_precio(e[x].precio_venta,e[x].mayoreo_1,e[x].mayoreo_2,e[x].mayoreo_3,e[x].mayoreo_4);
                      $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td width='1px' class='d-none'>"+e[x].id+
                        "</td>   <td>"+e[x].clave+
                        "</td>   <td>"+e[x].unidad+
                        "</td>   <td>"+e[x].descripcion_articulo+
                        "</td>   <td>"+precio+
                        "</td>   <td>"+e[x].existencia+
                        "</td>  </tr>");
                     }//fin for
                    }
                },error:function(){
                  alert("Hubo un problema al buscar este producto");
                }
              });  
          }
                       
      }

    
</script>
























