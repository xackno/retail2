@extends('layouts.app')

@section('content')
<br><br><br>
<div class="card">
	<div class="card-body">
		<h2 class="text-center text-primary"><i class="fas fa-list"></i> SALIDAS <i class="fas fa-hand-point-left "></i></h2>
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="table_salidas">
				<thead class="bg-primary text-white">
					<tr>
						<th>#</th>
						<th>usuario</th>
						<th>turno</th>
						<th>total</th>
						<th>concepto</th>
						<th>creado</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody>
					@foreach($salidas as $salida)
					<tr>
						<td>{{ str_pad($salida->id, 8, '0', STR_PAD_LEFT)}}</td>
						<td>{{$salida->name}}</td>
						<td>T{{ str_pad($salida->turno, 6, '0', STR_PAD_LEFT)}}</td>
						<td class="text-right"><strong>${{$salida->cantidad}}</strong> </td>
						<td>{{$salida->concepto}}</td>
						<td>{{$salida->created_at}}</td>
						<td></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>
<style type="text/css">
	.table tbody tr:hover{
		background:#3CA567;
		color:white;
	}
	.table td, .table th{
		padding: 0px;
		height: 30px
	}
</style>

@endsection
@section('script')
<script type="text/javascript">
	$("#table_salidas").DataTable({
		"order": [[ 0, 'desc' ]],
		"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
});

</script>
@endsection
