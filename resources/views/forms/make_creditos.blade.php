@extends('layouts.app')

@section('content')
<br><br><br>
<div class="card">
	<div class="card-header">
		<h2 style="margin: 0px;padding: 0px;width: 100%"><i class=" fas fa-shopping-cart"></i> venta a crédito 
			<a href="" class="btn btn-sm btn-primary float-right"><i class="fas fa-file-pdf"></i> REALIZAR EN PDF</a>
			<a href="" class="btn btn-sm btn-success float-right" style="margin-right: 5px"><i class="fas fa-ticket-alt"></i> REALIZAR EN TICKET</a>
		</h2>
	</div>
	<div class="card-body">
		<span class="badge badge-secondary"> CAJERO:{{Auth::user()->name}}</span>
		<span class="badge badge-success">TURNO:{{$turno}}</span>
		<span class="badge badge-primary"> CAJA:{{Auth::user()->caja}}</span>
		<span class="float-right text-primary">FECHA DE COMPRA: {{date('d-m-Y')}}</span>
		<hr style="border:1px solid black">

		<br>

		<div class="row">
			<div class="col col-sm-4 col-lg-4">
				<label>Selecciones un cliente</label>
				<select name="cliente" class="form-control  border-secondary">
					@foreach($clientes as $cliente)
					<option value="{{$cliente->id_cliente}}">{{$cliente->nombre_cliente}}</option>
					@endforeach
				</select>
				<div class="row">
					<div class="col-6">
						<label>Total:</label>
						<input type="text" name="total" class="form-control text-right  border-secondary" style="color:red" value="${{number_format(array_sum($subtotal),2, '.', '')}}" readonly="">
					</div>
					<div class="col-6">
						<label>Pago inicial requerido</label>
						@php
							$porcentaje=(array_sum($subtotal)/100)*10;
							$porcentaje=number_format($porcentaje,2, '.', '');
						@endphp
						<input type="text" class="form-control text-right border-secondary" name="pago_inicial" value="${{$porcentaje}}" readonly="">
					</div>
					<div class="col-6">
						<label>fecha límite</label>
						<select class="form-control" name="fecha_limite">
							<option value="0">0 meses</option>
							<option value="0">1 meses</option>
							<option value="0">2 meses</option>
							<option value="0">3 meses</option>
							<option value="0">4 meses</option>
							<option value="0">5 meses</option>
							<option value="0">6 meses</option>
							<option value="0">7 meses</option>
							<option value="0">8 meses</option>
							<option value="0">9 meses</option>
							<option value="0">10 meses</option>
							<option value="0">11 meses</option>
							<option value="0">12 meses</option>
						</select>
					</div>
				</div>
				

				

			</div>
			<div class="col col-sm-8 col-lg-8">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-primary text-white">
							<tr>
								<th>cantidad</th>
								<th>unidad</th>
								<th>descripción</th>
								<th>precio</th>
								<th>subtotal</th>
							</tr>
						</thead>
						<tbody>
							@for($x=0;$x< count($cantidad);$x++ )
							<tr>
								<td class="text-right">{{$cantidad[$x]}}</td>
								<td class="text-center">{{$unidad[$x]}}</td>
								<td>{{$descripcion[$x]}}</td>
								<td class="text-right">${{$precio[$x]}}</td>
								<td class="text-right">${{$subtotal[$x]}}</td>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<p style="font-size: 2em " class="float-right">Total:${{number_format(array_sum($subtotal), 2, '.', '')}}</p>
	</div>
</div>

<style type="text/css">
	.table tr th{padding:2px;height: 30px }
	.table tr td{padding:2px;height: 30px }
</style>

@endsection


@section('script')

@endsection