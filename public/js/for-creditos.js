$("#btn_credito").click(function(e){
	e.preventDefault();
    var id_p = [];
    var cantidad = [];
    var unidad = [];
    var descripcion = '';
    var precio = [];
    var subtotal = [];
    var cont = 0;
    $("#tabla_venta").find("tr td:first-child").each(function() {
        if (cont > 0) {
            id_p.push($(this).html());
            cantidad.push($(this).siblings("td").eq(0).html());
            unidad.push($(this).siblings("td").eq(1).html());
            descripcion += ($(this).siblings("td").eq(2).html()) + "=>";
            precio.push($(this).siblings("td").eq(3).html());
            subtotal.push($(this).siblings("td").eq(4).html());
        }
        cont++;
    });
    $("[name=credito_id_p]").val(id_p);
    $("[name=credito_cantidad]").val(cantidad);
    $("[name=credito_unidad]").val(unidad);
    $("[name=credito_descripcion]").val(descripcion);
    $("[name=credito_precio").val(precio);
    $("[name=credito_subtotal]").val(subtotal);
    
    $("#form_credito").submit();
});