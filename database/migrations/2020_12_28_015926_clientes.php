<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('clientes', function (Blueprint $table) {
            $table->id("id_cliente");
            $table->string('nombre_cliente');
            $table->string('identificacion')->nullable();
            $table->string('poblacion')->nullable();
            $table->string('colonia')->nullable();
            $table->string('calle')->nullable();
            $table->string('municipio')->nullable();
            $table->string('rfc')->nullable();
            $table->bigInteger('tel');
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
