<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ventas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id("id");
            $table->string('usuario');
            $table->string('id_turno');
            $table->integer("caja")->nullable();
            $table->string('efectivo');
            $table->decimal('total_venta', 10, 2);
            $table->text('cantidad_pro');
            $table->text('id_productos');
            $table->text("precio_vendido");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
