<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Articulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave');
            $table->text('descripcion_articulo');
            $table->string('unidad');
            $table->decimal('precio_compra',10,2);
            $table->decimal('precio_venta',10,2);
            $table->decimal('mayoreo_1',10,2)->nullable();
            $table->decimal('mayoreo_2',10,2)->nullable();
            $table->decimal('mayoreo_3',10,2)->nullable();
            $table->decimal('mayoreo_4',10,2)->nullable();
            $table->integer('codigo_sat')->nullable();
            $table->integer('proveedor')->nullable();
            $table->integer('categoria')->nullable();
            $table->integer('linea')->nullable();
            $table->integer('marca')->nullable();
            $table->decimal('existencia',10,2)->nullable();
            $table->integer('local')->nullable();
            $table->text('ubicacion_producto')->nullable();
            $table->text('fotos')->nullable();
            $table->text('palabra_clave')->nullable();
            $table->text('url')->nullable();
            $table->bigInteger('codigo_barra')->nullable();
            $table->date('caducidad')->nullable();
            $table->text('descripcion_catalogo')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
