<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       User::create([
            'name' => 'administrador',
            'user' => 'admin',
            'email' => 'xackno1995@gmail.com',
            'password' => Hash::make('admin123'),
            'type' => 'superadmin',
        ]);
    }
}
